# Virtual Schmirtual

Virtual Schmirtual is a tool to generate a menu-like html page from your nginx config file. From a hackathon in 2020 we bring you a niche tool. 

If you use your nginx server as a reverse proxy and haven't created a page that agrigates all of them in one place, this is for you.
What it does is accepts an nginx config file then outputs an html page you can host on your server to provide a convient menu to all of your 


Usage:
- compile the command  
  ```cargo build --release```
- move the commandfile
  - this generates a library and a linux command in target/release the command is called libvirtschmirt.so and virtual-schmirtual, repectively.  
  ```mv target/release/virtualschmirtual  ```  
  or  
  ```mv target/release/libvirtschmirt.so```  
- use the file  
  - You will have to pipe the command into a html file because the file is printed to the terminal (stdout).  
  ```./virtualschmirtual [/path/to/ngin.conf] > index.html```  
  or include libvirtschmirt.so in your c program and call it.

  ```char * c_read_config(char * string);```  
  where `string` is the contents of a nginx config file as a c_string. This returns an html file as a cstring that has a list of links.
  

