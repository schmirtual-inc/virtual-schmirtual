#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;

use std::collections::HashMap;

use rocket::request::Form;
use rocket::response::Redirect;
use rocket::http::{Cookie, Cookies};
use rocket_contrib::templates::Template;

#[derive(FromForm)]
struct Message {
    configfile: String,
}


#[get("/")]
fn index() -> Template {
    let mut context = HashMap::new();

    Template::render("index", &context)
}


#[get("/hello")]
fn hello() -> &'static str {
    "hello, World"
}

#[post("/generate_config")]
fn generate_config(task: Option<Form<Task>>) -> String {
    
}

fn rocket() -> rocket::Rocket {
    rocket::ignite().mount("/", routes![index, hello]).attach(Template::fairing())
}

fn main() {
    rocket().launch();    

}
