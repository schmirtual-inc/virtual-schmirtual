import os
from flask import Flask, request
from flask_cors import CORS
from werkzeug.utils import secure_filename
from cffi import FFI
import subprocess as sp
app = Flask(__name__)
CORS(app)


ffi = FFI()

# rust function declaration goes here
ffi.cdef("""
char * c_read_config(char * string);
   """)
   # not implemented yet

C = ffi.dlopen("../target/debug/libvirtschmirt.so")

@app.route('/generate_config', methods=['POST', 'GET'])
def generate_config():
   
   if request.method == 'POST':
      files = request.files.to_dict()
      if (len(files) < 1 or len(files) > 1):
         return "invalid number of files submitted", "400"
      config_file = ""
      for file in files:
         print(file, files[file])
         config_file = files[file]
         data = config_file.read()
         print(C.c_read_config(data))
         # files[file].read().decode("utf-8")
      #pipe this ^^^ into the parser
         # p1 = sp.Popen(["echo"], stdin=files[file])
         # p2 = sp.Popen([""])


   return ""


if __name__ == '__main__':
   app.run(debug=True)