// extern crate virtschmirt;

use std::env;
use virtschmirt as virt;

// extern 

pub fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() == 1 {
        println!("Not enough arguments!");
        return;
    }
    println!("args[1]: {}", args[1]);
    println!("{}", virt::read_config(args[1].clone()));
}
