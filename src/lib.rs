use std::fs::File;
use std::io::prelude::*;
use std::ffi::CString;
use std::os::raw::c_char;
use std::env;
use tera::{ Tera, Context };
use serde::{Serialize, Deserialize};

#[derive(Serialize)]
pub struct Location {
    fqdn: String,
    port: String,
    path: String,
    full_path: String,
}

fn new_location(fqdn: &String, port: &String, path: &String, full_path: &String) -> Location {
    Location {
        fqdn: fqdn.clone(),
        port: port.clone(),
        path: path.clone(),
        full_path: full_path.clone(),
    }
}


fn get_server_name(file_contents: &mut String) -> String {
    let line_end = file_contents.find(";").unwrap_or(file_contents.len());
    let fqdn = file_contents[file_contents.find("server_name ").unwrap_or(0) + "server_name ".len()..line_end].to_string();
    *file_contents = file_contents.split_at(line_end + 1).1.to_string();
    return fqdn;
}

fn get_port(file_contents: &mut String) -> String {
    let line_end = file_contents.find(";").unwrap_or(file_contents.len());
    let listen_string = &file_contents[file_contents.find("listen ").unwrap_or(0) + "listen ".len()..line_end].to_string();
    let port: String = listen_string[0..listen_string.find(" ").unwrap_or(listen_string.len())].to_string().clone();
    *file_contents = file_contents.split_at(line_end + 1).1.to_string();
    return port;
}



fn read_config_string(mut file_contents: String) -> String
{
    let file_pointer = &file_contents;
    let mut locationAcc: Vec::<Location> = Vec::new();
    let mut currentServerIndex = file_contents.find("server {");
    let mut totalServerIndex = 0;
    loop {
        match currentServerIndex {
            Some(csi) => {
                totalServerIndex += csi;
                let mut fqdn = String::new();
                let mut port = String::new();
                file_contents = file_contents[file_contents.find("server {").unwrap_or(0)..].to_string();
                if(file_contents.find("server_name ") < file_contents.find("listen ")) { // server_name comes before listen
                    fqdn = get_server_name(&mut file_contents);
                    port = get_port(&mut file_contents);

                }
                else {
                    port = get_port(&mut file_contents);
                    fqdn = get_server_name(&mut file_contents);

                }
                //println!("Found 'server_name' at position {}", totalServerIndex);

                // let listen_string = file_contents[file_contents.find("listen ").unwrap_or(0) + "listen ".len()..file_contents.find(";").unwrap_or(file_contents.len())].to_string();
                // let port = &listen_string[0..listen_string.find(" ").unwrap_or(listen_string.len())];
                if(fqdn == "_") {
                    println!("skipping invalid hostname {}", fqdn);
                    currentServerIndex = file_contents.find("server {");
                    continue;
                }

                file_contents = file_contents.split_at(file_contents.find(";").unwrap_or(0) + 1).1.to_string();
                let path = file_contents[file_contents.find("location ").unwrap_or(0) + "location ".len()..file_contents.find("{").unwrap_or(file_contents.len())].to_string();

                locationAcc.push(new_location(&fqdn, &port, &path, &"".to_string()));

                println!("FQDN: {}:{}{}", fqdn, port, path);

                file_contents = file_contents[file_contents.find("server {").unwrap_or(0)..file_contents.len()].to_string();
                currentServerIndex = file_contents.find("server {");
            },
            _ => { break; }
        }
    }
    make_html(&locationAcc)
}

fn make_html(locations: &Vec<Location>) -> String {
    let tera = match Tera::new("templates/*.html") {
        Ok(t) => t,
        Err(e) => {
            println!("tera error! {}", e);
            ::std::process::exit(1);
        },
    };
    let mut better_locations: Vec<Location> = Vec::new();
    for location in locations {
        let mut full_path = String::new();
        if(location.port == "443") {
            full_path.push_str("https://");
        }
        else {
            full_path.push_str("http://");
        }
        full_path.push_str(location.fqdn.as_str());
        full_path.push_str(location.path.as_str());
        better_locations.push(new_location(&location.fqdn, &location.port, &location.path, &full_path));
    }
    let mut context = Context::new();
    context.insert("locations", &better_locations);
    tera.render("index.html", &context).unwrap_or("".to_string())
}


pub fn read_config(filename: String) -> String{
    let mut file = File::open(filename).expect("error opening file");
    let mut file_contents = String::new();

    file.read_to_string(&mut file_contents);

    return read_config_string(file_contents);

}

#[no_mangle]
pub extern "C" fn c_read_config(string: CString) -> *const c_char
{

    let mut file_contents = String::new();
    let s = CString::new(read_config_string(string.into_string().unwrap())).unwrap();
    return s.as_ptr();
    
}
